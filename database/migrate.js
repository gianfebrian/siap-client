
const fs = require('fs');
const path = require('path');

const _ = require('lodash');
const squel = require('squel');

const connection = require('./db');

class Migration {
  constructor(db) {
    this.db = db;
  }

  isExists() {
    const query = 'SELECT * FROM migrations';

    try {
      this.db.querySync(query);

      return true;
    } catch (e) {
      return false;
    }
  }

  create() {
    const query = `
      CREATE TABLE migrations (
        id AUTOINCREMENT,
        name TEXT(100)
      )
    `;

    return this.db.querySync(query);
  }

  migrate() {
    const selectQuery = 'SELECT * FROM migrations';

    if (!this.isExists()) {
      this.create();
    }

    const selectResult = this.db.querySync(selectQuery);
    const migrated = _.map(selectResult, 'name');
    const scripts = fs.readdirSync(path.join(__dirname, 'migrations'));

    const migrations = _.reduce(scripts, (result, script) => {
      if (!_.includes(migrated, script)) {
        result.push({ name: script });

        return result;
      }

      return result;
    }, []);


    if (_.isEmpty(migrations)) {
      return console.log('No migration scripts were executed');  // eslint-disable-line
    }

    console.log('Executing migration scripts:'); // eslint-disable-line
    _.each(migrations, (migration) => {
      require(`./migrations/${migration.name}`)(this.db); // eslint-disable-line

      const insertQuery = squel.insert()
        .into('migrations')
        .setFields(migration)
        .toString();

      this.db.querySync(insertQuery);
      console.log(`* ${migration.name}`); // eslint-disable-line
    });

    return migrations;
  }
}

const migration = new Migration(connection);

module.exports = migration;
