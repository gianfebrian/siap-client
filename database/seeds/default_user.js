
const _ = require('lodash');
const squel = require('squel');
const bcrypt = require('bcrypt');

function generateHashedPassword(plainText) {
  return bcrypt.hashSync(plainText, bcrypt.genSaltSync(10));
}

function seed(db) {
  const defaultUsers = [
    {
      fullname: 'User Administrator',
      username: 'admin',
      password: generateHashedPassword('password'),
    },
  ];

  _.each(defaultUsers, (user) => {
    const insertQuery = squel.insert()
      .into('user')
      .setFields(user)
      .toString();

    db.querySync(insertQuery);
  });

  return defaultUsers;
}

module.exports = seed;
