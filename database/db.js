
const { Database } = require('odbc');

const config = require('./../server/config');

const dbFilePath = config.dbFilePath;
const connectionString = `Driver={Microsoft Access Driver (*.mdb, *.accdb)};Dbq=${dbFilePath};`;

class ODBC extends Database {
  constructor() {
    super();

    this.maxAttempt = 3;
  }

  connect(counter = 0) {
    try {
      this.openSync(connectionString);
    } catch (e) {
      if (counter > this.maxAttempt) {
        throw e;
      }

      const nextCounter = counter + 1;
      this.connect(nextCounter);
    }
  }

  querySync(query) {
    if (this.connected === false) {
      this.connect();
    }

    return super.querySync(query);
  }
}

const odbc = new ODBC();

module.exports = odbc;
