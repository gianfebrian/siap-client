
const fs = require('fs');
const path = require('path');

const _ = require('lodash');
const squel = require('squel');

const connection = require('./db');

class Seed {
  constructor(db) {
    this.db = db;
  }

  isExists() {
    const query = 'SELECT * FROM seeds';

    try {
      this.db.querySync(query);

      return true;
    } catch (e) {
      return false;
    }
  }

  create() {
    const query = `
      CREATE TABLE seeds (
        id AUTOINCREMENT,
        name TEXT(100)
      )
    `;

    return this.db.querySync(query);
  }

  seed() {
    const selectQuery = 'SELECT * FROM seeds';

    if (!this.isExists()) {
      this.create();
    }

    const selectResult = this.db.querySync(selectQuery);
    const migrated = _.map(selectResult, 'name');
    const scripts = fs.readdirSync(path.join(__dirname, 'seeds'));

    const seeds = _.reduce(scripts, (result, script) => {
      if (!_.includes(migrated, script)) {
        result.push({ name: script });

        return result;
      }

      return result;
    }, []);


    if (_.isEmpty(seeds)) {
      return console.log('No seed scripts were executed');  // eslint-disable-line
    }

    console.log('Executing seed scripts:'); // eslint-disable-line
    _.each(seeds, (seed) => {
      require(`./seeds/${seed.name}`)(this.db); // eslint-disable-line

      const insertQuery = squel.insert()
        .into('seeds')
        .setFields(seed)
        .toString();

      this.db.querySync(insertQuery);
      console.log(`* ${seed.name}`); // eslint-disable-line
    });

    return seeds;
  }
}

const seed = new Seed(connection);

module.exports = seed;
