
function migration(db) {
  const query = `
    CREATE TABLE achievement (
      achievement_id AUTOINCREMENT,
      student_id INTEGER,
      sport_type_id INTEGER,
      sport_sub_type TEXT(255),
      competition TEXT(255),
      competition_level TEXT(100),
      achievement TEXT(255)
    );
  `;

  return db.querySync(query);
}

module.exports = migration;
