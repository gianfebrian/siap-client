
function migration(db) {
  const query = `
    ALTER TABLE achievement ADD COLUMN media_id INTEGER
  `;

  return db.querySync(query);
}

module.exports = migration;
