
function migration(db) {
  const query = `
    CREATE TABLE media (
      media_id AUTOINCREMENT,
      filename TEXT(255),
      originalname TEXT(255),
      encoding TEXT(50),
      mimetype TEXT(100),
      size DOUBLE
    );
  `;

  return db.querySync(query);
}

module.exports = migration;
