
function migration(db) {
  const query = `
    CREATE TABLE school (
      school_id AUTOINCREMENT,
      name TEXT(255)
    );
  `;

  return db.querySync(query);
}

module.exports = migration;
