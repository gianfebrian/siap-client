
function migration(db) {
  const query = `
    CREATE TABLE student (
      student_id AUTOINCREMENT,
      school_id INTEGER,
      sport_type_id INTEGER,
      fullname TEXT(100),
      nisn TEXT(100),
      classroom TEXT(10),
      birth_place TEXT(50),
      birth_date DATE,
      address TEXT(255),
      sport_club TEXT(100)
    );
  `;

  return db.querySync(query);
}

module.exports = migration;
