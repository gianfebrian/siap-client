
function migration(db) {
  const query = `
    ALTER TABLE student ADD COLUMN parent TEXT(255)
  `;

  return db.querySync(query);
}

module.exports = migration;
