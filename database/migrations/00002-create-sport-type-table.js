
function migration(db) {
  const query = `
    CREATE TABLE sport_type (
      sport_type_id AUTOINCREMENT,
      name TEXT(255)
    );
  `;

  return db.querySync(query);
}

module.exports = migration;
