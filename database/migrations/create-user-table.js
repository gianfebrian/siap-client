
function migration(db) {
  const query = `
    CREATE TABLE user (
      id AUTOINCREMENT,
      fullname TEXT(100),
      username TEXT(255),
      password TEXT(255)
    );
  `;

  return db.querySync(query);
}

module.exports = migration;
