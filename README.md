
### How Install Distribution Binary on Windows ###

##### Minimum Installation Requirements: #####
1. Windows 7 or above (Use 64bit setup for 64 bit machine and 32bit setup for 32 bit machine)
2. Microsoft Access 2010 or above (make sure you have correct bit. Use 64 bit setup for Office 64 bit or 32 bit setup for Office 32 bit. Regardless your actual bit machine)
3. If you don't have Microsoft Access installation. Please download and install [Microsoft Access Database Engine 2010 Redistributable](https://www.microsoft.com/en-us/download/details.aspx?id=13255). Do not use 64 bit driver binary for 32 bit machine.

Now, you can run siap-bakminpor Setup {version}.exe

##### Issues: #####
1. Our binary (.exe) has not been signed. So, it will cause problem with Antivirus programs installed on target machine. Either, the setup cannot run at all or AV will run it first in sandbox mode. The solution is to add exclusions for our setup (recommended) or deactive antivirus before installation begin (not recommended).
2. As of now, port number being used by the application is static. So it may has chance to conflict with another service running on target machine. Solution, user should stop any service which using port `3333`.


### Development ###
Please note this first before continue reading:
1. Due to some problems of using environment variable on electron we use server/config.js to set application configurations.

You can set environment value inside server/config.js to development or production for development or distribute the binary respectively.

To run in development mode:

`````bash
> npm run build
> npm run electron
`````

To distribute binary:
`````bash
> npm run prod
> npm run dist
`````

If you want to change database schemata:
1. You can add one script inside database/migrations. Use previous template and change the DDL.
2. And then you can run `npm run migrate`

If you want to add default data to certain table on the database:
1. You can add one script inside database/seeds. Use previous template and change the DML.
2. And then you can run `npm run seed`

For code consistency we use eslint. So, please run `npm run lint .` before you push to remote branch or making distributable binary.

Note:
You can not edit your migration or seed script after "successfully" running `npm run migrate` or `npm run seed` since the script has recorded as migrated or seeded. So, you need to add another script to modify your script.

##### Known Bug: #####

1. accordion menu
  a. can not be active
  b. while in mini mode because of list a, it collapse normal instead of active in mini mode

