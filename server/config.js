
const os = require('os');
const path = require('path');

const config = {
  environment: 'production',

  runningPlatform: 'desktop',

  // application server port
  port: 3333,

  // token secret
  secret: 'SIAP_SECRET',

  // application user dir
  appUserDir: '.siap-bakminpor',

  // ms access database name
  db: 'data.accdb',

  // client configuration
  clientConfig: 'client.json',

  // upload dir
  uploadDir: 'upload',

  publicPath: path.join(__dirname, '..', 'public'),
};

config.publicUploadPath = path.join(os.homedir(), config.appUserDir, config.uploadDir);
config.clientConfigPath = path.join(os.homedir(), config.appUserDir, config.clientConfig);

// .mdb location
if (config.environment === 'production' && config.runningPlatform === 'desktop') {
  // previous value path.join(process.resourcesPath, 'database', config.db)
  config.dbFilePath = path.join(os.homedir(), config.appUserDir, config.db);
} else if (config.environment === 'development') {
  config.dbFilePath = path.join(__dirname, '..', 'database', config.db);
}

module.exports = config;
