
const { Router } = require('express');

const postLogin = require('./apis/post-login');

const postSchool = require('./apis/post-school');
const getSchool = require('./apis/get-school');
const getSchoolById = require('./apis/get-school-id');
const putSchool = require('./apis/put-school');
const deleteSchool = require('./apis/del-school');

const postSportType = require('./apis/post-sport-type');
const getSportType = require('./apis/get-sport-type');
const getSportTypeById = require('./apis/get-sport-type-id');
const putSportType = require('./apis/put-sport-type');
const deleteSportType = require('./apis/del-sport-type');

const postStudent = require('./apis/post-student');
const getStudent = require('./apis/get-student');
const getStudentById = require('./apis/get-student-id');
const putStudent = require('./apis/put-student');
const deleteStudent = require('./apis/del-student');

const postAchievement = require('./apis/post-achievement');
const getAchievement = require('./apis/get-achievement');
const getAchievementById = require('./apis/get-achievement-id');
const putAchievement = require('./apis/put-achievement');
const deleteAchievement = require('./apis/del-achievement');

const postUpload = require('./apis/post-upload');

const getReportStudent = require('./apis/get-report-student');
const getReportStudentDownload = require('./apis/get-report-student-download');

const getReportAchievement = require('./apis/get-report-achievement');
const getReportAchievementDownload = require('./apis/get-report-achievement-download');

const router = new Router();

router.post('/login', postLogin);

router.post('/school', postSchool);
router.get('/school', getSchool);
router.get('/school/:id', getSchoolById);
router.put('/school/:id', putSchool);
router.post('/school/delete', deleteSchool);

router.post('/sport-type', postSportType);
router.get('/sport-type', getSportType);
router.get('/sport-type/:id', getSportTypeById);
router.put('/sport-type/:id', putSportType);
router.post('/sport-type/delete', deleteSportType);

router.post('/student', postStudent);
router.get('/student', getStudent);
router.get('/student/:id', getStudentById);
router.put('/student/:id', putStudent);
router.post('/student/delete', deleteStudent);

router.post('/achievement', postAchievement);
router.get('/achievement', getAchievement);
router.get('/achievement/:id', getAchievementById);
router.put('/achievement/:id', putAchievement);
router.post('/achievement/delete', deleteAchievement);

router.post('/upload', postUpload);

router.get('/report/student', getReportStudent);
router.get('/report/student/download', getReportStudentDownload);

router.get('/report/achievement', getReportAchievement);
router.get('/report/achievement/download', getReportAchievementDownload);

module.exports = router;
