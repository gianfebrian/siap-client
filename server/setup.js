
const os = require('os');
const fs = require('fs');
const _ = require('lodash');
const path = require('path');

const config = require('./config');
const migration = require('./../database/migrate');

function setup(callback) {
  // we dont do initiate (check and process) this for development
  if (config.environement !== 'production' && config.runningPlatform !== 'desktop') {
    return callback();
  }

  // trying to list user home directory
  const listOfHomeDir = fs.readdirSync(os.homedir());

  // if specific app dir not found, create one
  if (!_.includes(listOfHomeDir, config.appUserDir)) {
    fs.mkdirSync(path.join(os.homedir(), config.appUserDir));
  }

  // trying to list app directory
  const listOfAppUserDir = fs.readdirSync(path.join(os.homedir(), config.appUserDir));

  const clientConfigTemplate = {
    requiredFilesCopied: false,
  };

  // create client configuration with default value if it does not exist
  if (!_.includes(listOfAppUserDir, config.clientConfig)) {
    fs.writeFileSync(config.clientConfigPath, JSON.stringify(clientConfigTemplate, null, 2));
  }

  const clientConfigData = JSON.parse(fs.readFileSync(config.clientConfigPath).toString());

  // if setup has been initialized don't do anything
  if (clientConfigData.requiredFilesCopied === true) {
    // todo: need to rework this
    migration.migrate(); // eslint-disable-line

    return callback();
  }

  // if upload dir has not been created, create one
  if (!_.includes(listOfAppUserDir, config.uploadDir)) {
    fs.mkdirSync(path.join(os.homedir(), config.appUserDir, config.uploadDir));
  }

  // if db has not been copied, copy one from the fresh source
  if (!_.includes(listOfAppUserDir, config.db)) {
    const copyStream = fs.createReadStream(path.join(__dirname, '..', 'database', config.db))
      .pipe(fs.createWriteStream(path.join(os.homedir(), config.appUserDir, config.db)));

    copyStream.on('finish', () => {
      // set flag, to tell setup has been initialized.
      clientConfigData.requiredFilesCopied = true;

      fs.writeFileSync(config.clientConfigPath, JSON.stringify(clientConfigData, null, 2));

      // todo: need to rework this
      migration.migrate(); // eslint-disable-line

      return callback();
    });
  }

  return callback();
}

module.exports = setup;
