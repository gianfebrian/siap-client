
const fs = require('fs');
const http = require('http');

const express = require('express');
const helmet = require('helmet');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const bearerToken = require('express-bearer-token');

const setup = require('./setup');
const config = require('./config');
const router = require('./router');

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(helmet());

app.use(morgan('combined'));

app.use(bearerToken({
  bodyKey: 'access_token',
  queryKey: 'access_token',
  headerKey: 'Bearer',
  reqKey: 'token',
}));

app.use(express.static(config.publicPath));
app.use(express.static(config.publicUploadPath));

app.use(router);

let server;

function start(callback) {
  let clientConfig = {};

  setup(() => {
    if (config.environment === 'production' && config.runningPlatform === 'desktop') {
      clientConfig = JSON.parse(fs.readFileSync(config.clientConfigPath).toString());
    }

    // use client based configuration or default port
    server = http.createServer(app);
    const port = clientConfig.port || config.port;
    server.listen(port, callback);
  });
}

function stop(callback) {
  server.close(callback);
}

module.exports = { start, stop };
