
const squel = require('squel');

const db = require('./../../database/db');

function putSchool(req, res) {
  const { id } = req.params;
  const payload = req.body;

  const updateQuery = squel.update()
    .table('school')
    .setFields(payload)
    .where('school_id = ?', Number(id))
    .toString();

  try {
    db.querySync(updateQuery);

    return res.json({ success: true });
  } catch (e) {
    return res.status(400).json({ success: false });
  }
}

module.exports = putSchool;
