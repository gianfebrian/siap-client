
const _ = require('lodash');
const squel = require('squel');

const db = require('./../../database/db');

function getSchoolById(req, res) {
  const { id } = req.params;

  const selectQuery = squel.select()
    .from('school')
    .where('school_id = ?', Number(id))
    .toString();

  try {
    const data = _.first(db.querySync(selectQuery));

    return res.json({ success: true, data });
  } catch (e) {
    return res.status(400).json({ success: false });
  }
}

module.exports = getSchoolById;
