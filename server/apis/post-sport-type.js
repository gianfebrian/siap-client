
const squel = require('squel');

const db = require('./../../database/db');

function postSportType(req, res) {
  const payload = req.body;

  const insertQuery = squel.insert()
    .into('sport_type')
    .setFields(payload)
    .toString();

  try {
    const data = db.querySync(insertQuery);

    return res.json({ success: true, data });
  } catch (e) {
    return res.status(400).json({ success: false });
  }
}

module.exports = postSportType;
