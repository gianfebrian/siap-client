
const _ = require('lodash');

const db = require('./../../database/db');

function getReportAchievement(req, res) {
  const { school_id, sport_type_id } = req.query;

  const criteria = [];
  if (!_.isEmpty(school_id)) {
    const criteriaBySchoolId = `student.school_id = ${Number(school_id)}`;

    criteria.push(criteriaBySchoolId);
  }

  if (!_.isEmpty(sport_type_id)) {
    const criteriaBySportTypeId = `sport_type.sport_type_id = ${Number(sport_type_id)}`;

    criteria.push(criteriaBySportTypeId);
  }

  let criteriaString = '';
  if (!_.isEmpty(criteria)) {
    criteriaString = ` WHERE ${criteria.join(' AND ')} `;
  }

  const selectQuery = `
    SELECT
      achievement.*,
      student.fullname AS student_fullname,
      school.name AS school_name,
      sport_type.name AS sport_type_name
    FROM
    (
      (
        (
          achievement
            INNER JOIN student
              ON achievement.student_id = student.student_id
        )
        INNER JOIN school
          ON student.school_id = school.school_id
      )
      INNER JOIN sport_type
        ON achievement.sport_type_id = sport_type.sport_type_id
    )
    ${criteriaString}
  `;

  try {
    const data = db.querySync(selectQuery);

    return res.json({ success: true, data });
  } catch (e) {
    return res.status(400).json({ success: false, error: JSON.stringify(e) });
  }
}

module.exports = getReportAchievement;
