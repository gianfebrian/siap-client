
const _ = require('lodash');
const squel = require('squel');

const db = require('./../../database/db');

function deleteSportType(req, res) {
  const { ids = [] } = req.body;

  const deleteQuery = squel.delete()
    .from('sport_type')
    .where('sport_type_id IN (?)', _.map(ids, id => Number(id)))
    .toString();

  try {
    db.querySync(deleteQuery);

    return res.json({ success: true });
  } catch (e) {
    return res.status(400).json({ success: false });
  }
}

module.exports = deleteSportType;
