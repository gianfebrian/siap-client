
const _ = require('lodash');

const db = require('./../../database/db');

function getAchievementById(req, res) {
  const { id } = req.params;

  const selectQuery = `
    SELECT
      achievement.*,
      student.fullname AS student_fullname,
      sport_type.name AS sport_type_name
    FROM
    (
      (
        achievement
          INNER JOIN student
            ON achievement.student_id = student.student_id
      )
      INNER JOIN sport_type
        ON achievement.sport_type_id = sport_type.sport_type_id
    )
    WHERE achievement.achievement_id = ${id}
  `;

  try {
    const data = _.first(db.querySync(selectQuery));

    return res.json({ success: true, data });
  } catch (e) {
    return res.status(400).json({ success: false, error: JSON.stringify(e) });
  }
}

module.exports = getAchievementById;
