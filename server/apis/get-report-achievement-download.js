
const _ = require('lodash');
const uuid = require('uuid');
const csv = require('fast-csv');

const db = require('./../../database/db');

function mapHeaders(row) {
  return {
    nama_siswa: row.student_fullname,
    sekolah: row.school_name,
    cabang_olahraga: row.sport_type_name,
    kejuaraan: row.competition,
    tingkat: row.competition_level,
    prestasi: row.achievement,
  };
}

function downloadAsCsv(res, data) {
  const filename = `siap-bakminpor-data-prestasi-${uuid.v4()}.csv`;
  res.setHeader('Content-disposition', `attachment; filename=${filename}`);
  res.setHeader('content-type', 'text/csv');

  const csvStream = csv.createWriteStream({ headers: true, objectMode: true });
  csvStream.pipe(res);
  _.each(data, row => csvStream.write(mapHeaders(row)));
  return csvStream.end();
}

// todo: handle format
function getReportAchievementDownload(req, res) {
  const { school_id, sport_type_id } = req.query;

  const criteria = [];
  if (!_.isEmpty(school_id)) {
    const criteriaBySchoolId = `student.school_id = ${Number(school_id)}`;

    criteria.push(criteriaBySchoolId);
  }

  if (!_.isEmpty(sport_type_id)) {
    const criteriaBySportTypeId = `sport_type.sport_type_id = ${Number(sport_type_id)}`;

    criteria.push(criteriaBySportTypeId);
  }

  let criteriaString = '';
  if (!_.isEmpty(criteria)) {
    criteriaString = ` WHERE ${criteria.join(' AND ')} `;
  }

  const selectQuery = `
    SELECT
      achievement.*,
      student.fullname AS student_fullname,
      school.name AS school_name,
      sport_type.name AS sport_type_name
    FROM
    (
      (
        (
          achievement
            INNER JOIN student
              ON achievement.student_id = student.student_id
        )
        INNER JOIN school
          ON student.school_id = school.school_id
      )
      INNER JOIN sport_type
        ON achievement.sport_type_id = sport_type.sport_type_id
    )
    ${criteriaString}
  `;

  try {
    const data = db.querySync(selectQuery);

    // default format
    return downloadAsCsv(res, data);
  } catch (e) {
    return res.status(400).json({ success: false });
  }
}

module.exports = getReportAchievementDownload;
