
const _ = require('lodash');
const squel = require('squel');

const db = require('./../../database/db');

function getSportTypeById(req, res) {
  const { id } = req.params;

  const selectQuery = squel.select()
    .from('sport_type')
    .where('sport_type_id = ?', Number(id))
    .toString();

  try {
    const data = _.first(db.querySync(selectQuery));

    return res.json({ success: true, data });
  } catch (e) {
    return res.status(400).json({ success: false, error: JSON.stringify(e) });
  }
}

module.exports = getSportTypeById;
