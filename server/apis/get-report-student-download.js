
const _ = require('lodash');
const uuid = require('uuid');
const csv = require('fast-csv');

const db = require('./../../database/db');

function mapHeaders(row) {
  return {
    nama_siswa: row.fullname,
    sekolah: row.school_name,
    nisn: row.nisn,
    kelas: row.classroom,
    cabang_olahraga: row.sport_type_name,
    klub_olahraga: row.sport_club,
  };
}

function downloadAsCsv(res, data) {
  const filename = `siap-bakminpor-data-siswa-${uuid.v4()}.csv`;
  res.setHeader('Content-disposition', `attachment; filename=${filename}`);
  res.setHeader('content-type', 'text/csv');

  const csvStream = csv.createWriteStream({ headers: true, objectMode: true });
  csvStream.pipe(res);

  _.each(data, row => csvStream.write(mapHeaders(row)));

  return csvStream.end();
}

// todo: handle format
function getReportStudentDownload(req, res) {
  const { school_id, sport_type_id } = req.query;

  const criteria = [];
  if (!_.isEmpty(school_id)) {
    const criteriaBySchoolId = `student.school_id = ${Number(school_id)}`;

    criteria.push(criteriaBySchoolId);
  }

  if (!_.isEmpty(sport_type_id)) {
    const criteriaBySportTypeId = `sport_type.sport_type_id = ${Number(sport_type_id)}`;

    criteria.push(criteriaBySportTypeId);
  }

  let criteriaString = '';
  if (!_.isEmpty(criteria)) {
    criteriaString = ` WHERE ${criteria.join(' AND ')} `;
  }

  const selectQuery = `
    SELECT
      student.*,
      sport_type.name AS sport_type_name,
      school.name AS school_name
    FROM
    (
      (
        student
          INNER JOIN sport_type
            ON student.sport_type_id = sport_type.sport_type_id
      )
      INNER JOIN school
        ON student.school_id = school.school_id
    )
    ${criteriaString}
  `;

  try {
    const data = db.querySync(selectQuery);

    // default format
    return downloadAsCsv(res, data);
  } catch (e) {
    return res.status(400).json({ success: false });
  }
}

module.exports = getReportStudentDownload;
