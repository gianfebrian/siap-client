
const squel = require('squel');
const moment = require('moment');

const db = require('./../../database/db');

function putStudent(req, res) {
  const { id } = req.params;
  const payload = req.body;

  payload.birth_date = moment(payload.moment).format('YYYY-MM-DD');

  const updateQuery = squel.update()
    .table('student')
    .setFields(payload)
    .where('student_id = ?', Number(id))
    .toString();

  updateQuery.replace(payload.birth_date, `d'${payload.birth_date}'`);

  try {
    db.querySync(updateQuery);

    return res.json({ success: true });
  } catch (e) {
    return res.status(400).json({ success: false });
  }
}

module.exports = putStudent;
