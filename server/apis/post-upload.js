
const fs = require('fs');
const path = require('path');

const _ = require('lodash');
const squel = require('squel');
const multer = require('multer');

const config = require('./../config');

const storage = multer.diskStorage({});

const upload = multer({ storage }).single('file');

const db = require('./../../database/db');

function postUpload(req, res) {
  upload(req, res, (err) => {
    if (err) {
      return res.status(400).json({ success: false });
    }

    const originalFileExt = _.first(req.file.originalname.match(/[^.]+(?!\.\w)$/g));
    const targetFilename = `${req.file.filename}.${originalFileExt}`;
    const targetFilepath = path.join(config.publicUploadPath, targetFilename);

    fs.createReadStream(req.file.path)
      .pipe(fs.createWriteStream(targetFilepath));

    try {
      const insertQuery = squel.insert()
        .into('media')
        .setFields({
          filename: req.file.filename,
          originalname: req.file.originalname,
          encoding: req.file.encoding,
          mimetype: req.file.mimetype,
          size: req.file.size,
        })
        .toString();

      db.querySync(insertQuery);

      const returningSelectQuery = squel.select()
        .from('media')
        .where('filename = ?', req.file.filename)
        .toString();

      const data = _.first(db.querySync(returningSelectQuery));

      return res.json({ success: true, data });
    } catch (e) {
      return res.json({ success: false });
    }
  });
}

module.exports = postUpload;
