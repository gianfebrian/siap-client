
const squel = require('squel');
const moment = require('moment');

const db = require('./../../database/db');

function postStudent(req, res) {
  const payload = req.body;

  payload.birth_date = moment(payload.moment).format('YYYY-MM-DD');

  const insertQuery = squel.insert()
    .into('student')
    .setFields(payload)
    .toString();

  // ms access hack for date
  insertQuery.replace(payload.birth_date, `d'${payload.birth_date}'`);

  try {
    const data = db.querySync(insertQuery);

    return res.json({ success: true, data });
  } catch (e) {
    return res.status(400).json({ success: false });
  }
}

module.exports = postStudent;
