
const _ = require('lodash');

const db = require('./../../database/db');

function getReportStudent(req, res) {
  const { school_id, sport_type_id } = req.query;

  const criteria = [];
  if (!_.isEmpty(school_id)) {
    const criteriaBySchoolId = `student.school_id = ${Number(school_id)}`;

    criteria.push(criteriaBySchoolId);
  }

  if (!_.isEmpty(sport_type_id)) {
    const criteriaBySportTypeId = `sport_type.sport_type_id = ${Number(sport_type_id)}`;

    criteria.push(criteriaBySportTypeId);
  }

  let criteriaString = '';
  if (!_.isEmpty(criteria)) {
    criteriaString = ` WHERE ${criteria.join(' AND ')} `;
  }

  const selectQuery = `
    SELECT
      student.*,
      sport_type.name AS sport_type_name,
      school.name AS school_name
    FROM
    (
      (
        student
          INNER JOIN sport_type
            ON student.sport_type_id = sport_type.sport_type_id
      )
      INNER JOIN school
        ON student.school_id = school.school_id
    )
    ${criteriaString}
  `;

  try {
    const data = db.querySync(selectQuery);

    return res.json({ success: true, data });
  } catch (e) {
    return res.status(400).json({ success: false });
  }
}

module.exports = getReportStudent;
