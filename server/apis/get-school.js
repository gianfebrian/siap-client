
const squel = require('squel');

const db = require('./../../database/db');

function getSchool(req, res) {
  const selectQuery = squel.select()
    .from('school')
    .toString();

  try {
    const data = db.querySync(selectQuery);

    return res.json({ success: true, data });
  } catch (e) {
    return res.status(400).json({ success: false, error: JSON.stringify(e) });
  }
}

module.exports = getSchool;
