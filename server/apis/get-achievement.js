
const db = require('./../../database/db');

function getAchievement(req, res) {
  const selectQuery = `
    SELECT
      achievement.*,
      student.fullname AS student_fullname,
      sport_type.name AS sport_type_name,
      media.*
    FROM
    (
      (
        (
          achievement
            INNER JOIN student
              ON achievement.student_id = student.student_id
        )
        INNER JOIN sport_type
          ON achievement.sport_type_id = sport_type.sport_type_id
      )
      LEFT JOIN media
        ON achievement.media_id = media.media_id
    )
  `;

  try {
    const data = db.querySync(selectQuery);

    return res.json({ success: true, data });
  } catch (e) {
    return res.status(400).json({ success: false, error: JSON.stringify(e) });
  }
}

module.exports = getAchievement;
