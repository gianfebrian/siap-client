
const _ = require('lodash');
const squel = require('squel');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const config = require('./../config');
const db = require('./../../database/db');

function login(req, res) {
  const { username, password } = req.body;

  const query = squel.select()
    .from('user')
    .where('username = ?', username)
    .toString();

  const result = _.first(db.querySync(query));

  if (!_.isEmpty(result)) {
    const isValid = bcrypt.compareSync(password, result.password);

    if (isValid) {
      const authToken = jwt.sign({ simple: true }, config.secret);

      return res.json({ success: true, authToken });
    }
  }

  return res.status(401).json({ success: false });
}

module.exports = login;
