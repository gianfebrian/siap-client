
const _ = require('lodash');

const db = require('./../../database/db');

function getStudentById(req, res) {
  const { id } = req.params;

  const selectQuery = `
    SELECT
      student.*,
      sport_type.name AS sport_type_name,
      school.name AS school_name
    FROM
    (
      (
        student
          INNER JOIN sport_type
            ON student.sport_type_id = sport_type.sport_type_id
      )
      INNER JOIN school
        ON student.school_id = school.school_id
    )
    WHERE student.student_id = ${Number(id)}
  `;

  try {
    const data = _.first(db.querySync(selectQuery));

    return res.json({ success: true, data });
  } catch (e) {
    return res.status(400).json({ success: false });
  }
}

module.exports = getStudentById;
