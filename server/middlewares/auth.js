
const jwt = require('jsonwebtoken');

const config = require('./../config');

function auth(req, res, next) {
  const { token } = req;

  jwt.verify(token, config.secret, (err) => {
    if (err) {
      return res.status(401).send('Unauthorized');
    }

    return next();
  });
}

module.exports = auth;
