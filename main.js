
const electron = require('electron'); // eslint-disable-line

const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

const server = require('./server');

let mainWindow;

function createWindow() {
  // setup();

  // Create the browser window.
  mainWindow = new BrowserWindow({ width: 800, height: 600 });
  mainWindow.setMenu(null);

  server.start(() => {
    const session = mainWindow.webContents.session;
    session.clearStorageData(() => {
      // and load the index.html of the app.
      mainWindow.loadURL('http://localhost:3333');
    });
  });

  mainWindow.on('closed', () => {
    mainWindow = null;
  });
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});
