
const { platformBrowserDynamic } = require('@angular/platform-browser-dynamic');
const { AppModule } = require('./app/app.module');

platformBrowserDynamic().bootstrapModule(AppModule);
