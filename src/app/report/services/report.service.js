/* global Blob */

const _ = require('lodash');
const FileSaver = require('file-saver');


const { Injectable } = require('@angular/core');
const { Http, URLSearchParams } = require('@angular/http');

const { RequestService } = require('./../../auth/services/request/request.service');

class ReportService {
  constructor(http, requestService) {
    this.http = http;
    this.requestService = requestService;

    this.headers = this.requestService.getAuthHeaders();
  }

  searchStudentListByCriteria({ school_id, sport_type_id } = {}) {
    const search = new URLSearchParams();
    search.set('school_id', school_id);
    search.set('sport_type_id', sport_type_id);

    return this.http
      .get('/report/student', { headers: this.headers, search })
      .map(res => res.json())
      .map((res) => {
        const { success, data } = res;

        return { success, data };
      });
  }

  saveStudentList({ school_id, sport_type_id, format } = {}) {
    const search = new URLSearchParams();
    search.set('school_id', school_id);
    search.set('sport_type_id', sport_type_id);
    search.set('format', format);

    let responseType = '';
    if (format === 'csv') {
      responseType = 'text/csv';
    }

    return this.http
      .get('/report/student/download', { headers: this.headers, search })
      .subscribe((result) => {
        const headers = result.headers;
        const filename = _.first(headers.get('Content-Disposition')
          .match(/filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/g));

        const blob = new Blob([result._body], { type: responseType }); // eslint-disable-line

        FileSaver.saveAs(blob, filename.replace('filename=', ''));
      });
  }

  searchAchievementListByCriteria({ school_id, sport_type_id } = {}) {
    const search = new URLSearchParams();
    search.set('school_id', school_id);
    search.set('sport_type_id', sport_type_id);

    return this.http
      .get('/report/achievement', { headers: this.headers, search })
      .map(res => res.json())
      .map((res) => {
        const { success, data } = res;

        return { success, data };
      });
  }

  saveAchievementList({ school_id, sport_type_id, format } = {}) {
    const search = new URLSearchParams();
    search.set('school_id', school_id);
    search.set('sport_type_id', sport_type_id);
    search.set('format', format);

    let responseType = '';
    if (format === 'csv') {
      responseType = 'text/csv';
    }

    return this.http
      .get('/report/achievement/download', { headers: this.headers, search })
      .subscribe((result) => {
        const headers = result.headers;
        const filename = _.first(headers.get('Content-Disposition')
          .match(/filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/g));

        const blob = new Blob([result._body], { type: responseType }); // eslint-disable-line

        FileSaver.saveAs(blob, filename.replace('filename=', ''));
      });
  }
}

ReportService.annotations = [
  new Injectable(),
];

ReportService.parameters = [
  [Http],
  [RequestService],
];

module.exports = { ReportService };

