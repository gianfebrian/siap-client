
const { ReportStudentComponent } = require('./components/report-student/report-student.component');
const { ReportAchievementComponent } = require('./components/report-achievement/report-achievement.component');

const { ReportService } = require('./services/report.service');

module.exports = {
  REPORT_DECLARATIONS: [
    ReportStudentComponent,
    ReportAchievementComponent,
  ],

  REPORT_PROVIDERS: [
    ReportService,
  ],
};
