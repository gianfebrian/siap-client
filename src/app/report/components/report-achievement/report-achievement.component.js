
const _ = require('lodash');

const { Component } = require('@angular/core');
const { BehaviorSubject } = require('rxjs/BehaviorSubject');

const { ReportService } = require('./../../services/report.service');

const { SchoolService } = require('./../../../school/services/school.service');
const { SportTypeService } = require('./../../../sport-type/services/sport-type.service');


const template = require('./report-achievement.template.html');

class ReportAchievementComponent {
  constructor(reportService, schoolService, sportTypeService) {
    this.infoMessage = null;
    this.rows = new BehaviorSubject([]);

    this.reportService = reportService;

    this.schoolService = schoolService;
    this.schoolOptions = new BehaviorSubject([]);
    this.selectedSchool = '';
    this.sportTypeService = sportTypeService;
    this.sportTypeOptions = new BehaviorSubject([]);
    this.selectedSportType = '';

    this.columns = [
      { name: 'Nama Siswa', prop: 'student_fullname' },
      { name: 'Sekolah', prop: 'school_name' },
      { name: 'Cabang Olahraga', prop: 'sport_type_name' },
      { name: 'Sub Cabang', prop: 'sport_sub_type' },
      { name: 'Kejuaraan', prop: 'competition' },
      { name: 'Tingkat', prop: 'competition_level' },
      { name: 'Prestasi', prop: 'achievement' },
    ];
  }

  ngOnInit() {
    this.schoolService.getSelectionOptions()
      .subscribe(options => this.schoolOptions.next(options));

    this.sportTypeService.getSelectionOptions()
      .subscribe(options => this.sportTypeOptions.next(options));
  }

  onSearch() {
    this.reportService.searchAchievementListByCriteria({
      school_id: this.selectedSchool,
      sport_type_id: this.selectedSportType,
    })
      .subscribe((result) => {
        if (_.isEmpty(result.data)) {
          return this.rows;
        }

        return this.rows.next(result.data);
      });
  }

  onSaveAsExcel() {
    this.reportService.saveAchievementList({
      school_id: this.selectedSchool,
      sport_type_id: this.selectedSportType,
      format: 'csv',
    });
  }
}

ReportAchievementComponent.annotations = [
  new Component({
    template,
    selector: 'report-achievement',
  }),
];

ReportAchievementComponent.parameters = [
  [ReportService],
  [SchoolService],
  [SportTypeService],
];

module.exports = { ReportAchievementComponent };
