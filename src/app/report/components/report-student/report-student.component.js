
const _ = require('lodash');

const { Component } = require('@angular/core');
const { BehaviorSubject } = require('rxjs/BehaviorSubject');

const { ReportService } = require('./../../services/report.service');

const { SchoolService } = require('./../../../school/services/school.service');
const { SportTypeService } = require('./../../../sport-type/services/sport-type.service');


const template = require('./report-student.template.html');

class ReportStudentComponent {
  constructor(reportService, schoolService, sportTypeService) {
    this.infoMessage = null;
    this.rows = new BehaviorSubject([]);

    this.reportService = reportService;

    this.schoolService = schoolService;
    this.schoolOptions = new BehaviorSubject([]);
    this.selectedSchool = '';
    this.sportTypeService = sportTypeService;
    this.sportTypeOptions = new BehaviorSubject([]);
    this.selectedSportType = '';

    this.columns = [
      { name: 'Nama Siswa', prop: 'fullname' },
      { name: 'Sekolah', prop: 'school_name' },
      { name: 'NISN', prop: 'nisn' },
      { name: 'Kelas', prop: 'classroom' },
      { name: 'Cabang Olahraga', prop: 'sport_type_name' },
      { name: 'Klub Olahraga', prop: 'sport_club' },
    ];
  }

  ngOnInit() {
    this.schoolService.getSelectionOptions()
      .subscribe(options => this.schoolOptions.next(options));

    this.sportTypeService.getSelectionOptions()
      .subscribe(options => this.sportTypeOptions.next(options));
  }

  onSearch() {
    this.reportService.searchStudentListByCriteria({
      school_id: this.selectedSchool,
      sport_type_id: this.selectedSportType,
    })
      .subscribe((result) => {
        if (_.isEmpty(result.data)) {
          return this.rows;
        }

        return this.rows.next(result.data);
      });
  }

  onSaveAsExcel() {
    this.reportService.saveStudentList({
      school_id: this.selectedSchool,
      sport_type_id: this.selectedSportType,
      format: 'csv',
    });
  }
}

ReportStudentComponent.annotations = [
  new Component({
    template,
    selector: 'report-student',
  }),
];

ReportStudentComponent.parameters = [
  [ReportService],
  [SchoolService],
  [SportTypeService],
];

module.exports = { ReportStudentComponent };
