
const { Component } = require('@angular/core');

const template = require('./page-home.template.html');

class PageHomeComponent {}

PageHomeComponent.annotations = [
  new Component({
    template,
    selector: 'page-home',
  }),
];

module.exports = { PageHomeComponent };
