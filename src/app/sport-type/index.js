
const { SportTypeListComponent } = require('./components/sport-type-list/sport-type-list.component');
const { SportTypeFormComponent } = require('./components/sport-type-form/sport-type-form.component');
const { SportTypeAddFormComponent } = require('./components/sport-type-add-form/sport-type-add-form.component');
const { SportTypeEditFormComponent } = require('./components/sport-type-edit-form/sport-type-edit-form.component');

const { SportTypeService } = require('./services/sport-type.service');

module.exports = {
  SPORT_TYPE_DECLARATIONS: [
    SportTypeListComponent,
    SportTypeFormComponent,
    SportTypeAddFormComponent,
    SportTypeEditFormComponent,
  ],

  SPORT_TYPE_PROVIDERS: [
    SportTypeService,
  ],
};
