
const { Component } = require('@angular/core');
const { ActivatedRoute, Router } = require('@angular/router');

const { BehaviorSubject } = require('rxjs/BehaviorSubject');

const { SportTypeService } = require('./../../services/sport-type.service');

const template = require('./sport-type-edit-form.template.html');

class SportTypeEditFormComponent {
  constructor(sportTypeService, activatedRoute, router) {
    this.activatedRoute = activatedRoute;
    this.router = router;
    this.sportTypeService = sportTypeService;

    this.sportTypeId = null;
    this.data = new BehaviorSubject({});
  }

  ngOnInit() {
    this.activatedRoute.params
      .subscribe((params) => {
        this.sportTypeId = params.id;
        this.sportTypeService.getSportTypeById(this.sportTypeId)
          .subscribe(result => this.data.next(result.data));
      });
  }

  onSave(data) {
    this.sportTypeService.updateSportType(this.sportTypeId, data).subscribe((result) => {
      if (result) {
        this.router.navigate(['/sport-type-list']);
      }
    });
  }
}

SportTypeEditFormComponent.annotations = [
  new Component({
    template,
    selector: 'sport-type-edit-form',
  }),
];

SportTypeEditFormComponent.parameters = [
  [SportTypeService],
  [ActivatedRoute],
  [Router],
];

module.exports = { SportTypeEditFormComponent };
