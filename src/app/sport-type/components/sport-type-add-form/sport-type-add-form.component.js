
const { Component } = require('@angular/core');
const { Router } = require('@angular/router');

const { SportTypeService } = require('./../../services/sport-type.service');

const template = require('./sport-type-add-form.template.html');

class SportTypeAddFormComponent {
  constructor(sportTypeService, router) {
    this.router = router;
    this.sportTypeService = sportTypeService;
  }

  onSave(data) {
    this.sportTypeService.addSportType(data).subscribe((result) => {
      if (result) {
        this.router.navigate(['/sport-type-list']);
      }
    });
  }
}

SportTypeAddFormComponent.annotations = [
  new Component({
    template,
    selector: 'sport-type-add-form',
  }),
];

SportTypeAddFormComponent.parameters = [
  [SportTypeService],
  [Router],
];

module.exports = { SportTypeAddFormComponent };
