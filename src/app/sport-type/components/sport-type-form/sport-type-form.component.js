
const { Component, EventEmitter } = require('@angular/core');
const { FormBuilder, Validators } = require('@angular/forms');

const template = require('./sport-type-form.template.html');

class SportTypeFormComponent {
  constructor(formBuilder) {
    this.data = null;
    this.onSave = new EventEmitter();

    this.sportTypeForm = formBuilder.group({
      name: ['', [Validators.required]],
    });
  }

  ngOnChanges(changedValue) {
    const { name } = changedValue.data.currentValue;
    this.sportTypeForm.controls.name.setValue(name);
  }

  onSubmit(data) {
    this.onSave.emit(data);
  }
}

SportTypeFormComponent.annotations = [
  new Component({
    template,
    selector: 'sport-type-form',
    inputs: [
      'data',
    ],
    outputs: [
      'onSave',
    ],
  }),
];

SportTypeFormComponent.parameters = [
  [FormBuilder],
];

module.exports = { SportTypeFormComponent };
