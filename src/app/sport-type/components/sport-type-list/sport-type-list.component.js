
const _ = require('lodash');

const { Router } = require('@angular/router');
const { Component } = require('@angular/core');
const { BehaviorSubject } = require('rxjs/BehaviorSubject');

const { SportTypeService } = require('./../../services/sport-type.service');

const template = require('./sport-type-list.template.html');

class SportTypeListComponent {
  constructor(router, sportTypeService) {
    this.selected = [];
    this.infoMessage = null;
    this.rows = new BehaviorSubject([]);

    this.router = router;

    this.sportTypeService = sportTypeService;
  }

  ngOnInit() {
    this.getTableData();
  }

  getTableData() {
    this.sportTypeService.getListOfSportType().subscribe((result) => {
      if (_.isEmpty(result.data)) {
        return this.rows;
      }

      // reset selected checkbox
      this.selected.splice(0, this.selected.length);
      return this.rows.next(result.data);
    });
  }

  onSelect({ selected } = {}) {
    this.selected.splice(0, this.selected.length);

    this.selected.push(...selected);
  }

  onEditButtonClick() {
    if (this.selected.length === 0) {
      this.infoMessage = 'Silahkan memilih data untuk melakukan perubahan';
      return;
    }

    if (this.selected.length > 1) {
      this.infoMessage = 'Anda memilih lebih dari 1 data. Jumlah pilihan yang diijinkan adalah 1 data.';
      return;
    }

    const selected = _.first(this.selected);
    this.router.navigate([`/sport-type-edit-form/${selected.sport_type_id}`]);
  }

  onRemoveButtonClick() {
    if (this.selected.length === 0) {
      this.infoMessage = 'Silahkan memilih data untuk melakukan penghapusan';
    }

    const ids = _.map(this.selected, 'sport_type_id');
    this.sportTypeService.deleteSportType(ids)
      .subscribe(() => this.getTableData());
  }
}

SportTypeListComponent.annotations = [
  new Component({
    template,
    selector: 'sport-type-list',
  }),
];

SportTypeListComponent.parameters = [
  [Router],
  [SportTypeService],
];

module.exports = { SportTypeListComponent };
