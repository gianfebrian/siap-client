
const _ = require('lodash');

const { Injectable } = require('@angular/core');
const { Http } = require('@angular/http');

const { RequestService } = require('./../../auth/services/request/request.service');

class SportTypeService {
  constructor(http, requestService) {
    this.http = http;
    this.requestService = requestService;

    this.headers = this.requestService.getAuthHeaders();
  }

  addSportType(data) {
    const payload = JSON.stringify(data);

    return this.http
      .post('/sport-type', payload, { headers: this.headers })
      .map(res => res.json())
      .map(res => res.success);
  }

  getListOfSportType() {
    return this.http
      .get('/sport-type', { headers: this.headers })
      .map(res => res.json())
      .map((res) => {
        const { success, data } = res;

        return { success, data };
      });
  }

  getSportTypeById(id) {
    return this.http
      .get(`/sport-type/${id}`, { headers: this.headers })
      .map(res => res.json())
      .map((res) => {
        const { success, data } = res;

        return { success, data };
      });
  }

  updateSportType(id, data) {
    const payload = JSON.stringify(data);

    return this.http
      .put(`/sport-type/${id}`, payload, { headers: this.headers })
      .map(res => res.json())
      .map(res => res.success);
  }

  getSelectionOptions() {
    return this.getListOfSportType().map((result) => {
      if (_.isEmpty(result.data)) {
        return [];
      }

      const options = _.map(result.data, dataObject => (
        {
          label: dataObject.name,
          value: dataObject.sport_type_id,
        }
      ));

      return options;
    });
  }

  deleteSportType(ids = []) {
    const payload = JSON.stringify({ ids });

    return this.http
      .post('/sport-type/delete', payload, { headers: this.headers })
      .map(res => res.json())
      .map(res => res.success);
  }
}

SportTypeService.annotations = [
  new Injectable(),
];

SportTypeService.parameters = [
  [Http],
  [RequestService],
];

module.exports = { SportTypeService };
