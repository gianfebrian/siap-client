
const { AppComponent } = require('./components/app/app.component');

const { LoggedInGuard } = require('./guards/logged-in.guard');
const { LoggedOutGuard } = require('./guards/logged-out.guard');

const { HeaderMainComponent } = require('./components/header-main/header-main.component');
const { HeaderNavbarComponent } = require('./components/header-navbar/header-navbar.component');
const { HeaderNavbarControlComponent } = require('./components/header-navbar-control/header-navbar-control.component');
const { HeaderNavbarMessageComponent } = require('./components/header-navbar-message/header-navbar-message.component');
const { HeaderNavbarNotificationComponent } = require('./components/header-navbar-notification/header-navbar-notification.component');
const { HeaderNavbarTaskComponent } = require('./components/header-navbar-task/header-navbar-task.component');
const { HeaderNavbarUserComponent } = require('./components/header-navbar-user/header-navbar-user.component');

const { SidebarMainComponent } = require('./components/sidebar-main/sidebar-main.component');
const { SidebarMenuComponent } = require('./components/sidebar-menu/sidebar-menu.component');
const { SidebarSearchComponent } = require('./components/sidebar-search/sidebar-search.component');
const { SidebarUserPanelComponent } = require('./components/sidebar-user-panel/sidebar-user-panel.component');

module.exports = {
  AppComponent,

  CORE_PROVIDERS: [
    LoggedInGuard,
    LoggedOutGuard,
  ],

  CORE_DECLARATIONS: [
    HeaderMainComponent,
    HeaderNavbarComponent,
    HeaderNavbarControlComponent,
    HeaderNavbarMessageComponent,
    HeaderNavbarNotificationComponent,
    HeaderNavbarTaskComponent,
    HeaderNavbarUserComponent,

    SidebarMainComponent,
    SidebarMenuComponent,
    SidebarSearchComponent,
    SidebarUserPanelComponent,
  ],
};
