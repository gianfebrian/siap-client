
const { LoggedInGuard } = require('./guards/logged-in.guard');
const { LoggedOutGuard } = require('./guards/logged-out.guard');

const { HeaderMainComponent } = require('./components/header-main/header-main.component');
const { SidebarMainComponent } = require('./components/sidebar-main/sidebar-main.component');

const { LoginComponent } = require('./../auth/components/login/login.component');
const { PageHomeComponent } = require('./../pages/components/page-home/page-home.component');

const { StudentListComponent } = require('./../student/components/student-list/student-list.component');
const { StudentAddFormComponent } = require('./../student/components/student-add-form/student-add-form.component');
const { StudentEditFormComponent } = require('./../student/components/student-edit-form/student-edit-form.component');

const { AchievementListComponent } = require('./../achievement/components/achievement-list/achievement-list.component');
const { AchievementAddFormComponent } = require('./../achievement/components/achievement-add-form/achievement-add-form.component');
const { AchievementEditFormComponent } = require('./../achievement/components/achievement-edit-form/achievement-edit-form.component');

const { SchoolListComponent } = require('./../school/components/school-list/school-list.component');
const { SchoolAddFormComponent } = require('./../school/components/school-add-form/school-add-form.component');
const { SchoolEditFormComponent } = require('./../school/components/school-edit-form/school-edit-form.component');

const { SportTypeListComponent } = require('./../sport-type/components/sport-type-list/sport-type-list.component');
const { SportTypeAddFormComponent } = require('./../sport-type/components/sport-type-add-form/sport-type-add-form.component');
const { SportTypeEditFormComponent } = require('./../sport-type/components/sport-type-edit-form/sport-type-edit-form.component');

const { ReportStudentComponent } = require('./../report/components/report-student/report-student.component');
const { ReportAchievementComponent } = require('./../report/components/report-achievement/report-achievement.component');

const routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [LoggedOutGuard],
  },
  {
    path: 'home',
    children: [
      {
        path: '',
        component: PageHomeComponent,
      },
      {
        path: '',
        component: HeaderMainComponent,
        outlet: 'header',
      },
      {
        path: '',
        component: SidebarMainComponent,
        outlet: 'sidebar',
      },
    ],
    canActivate: [LoggedInGuard],
  },
  {
    path: 'student-list',
    children: [
      {
        path: '',
        component: StudentListComponent,
      },
      {
        path: '',
        component: HeaderMainComponent,
        outlet: 'header',
      },
      {
        path: '',
        component: SidebarMainComponent,
        outlet: 'sidebar',
      },
    ],
  },
  {
    path: 'student-add-form',
    children: [
      {
        path: '',
        component: StudentAddFormComponent,
      },
      {
        path: '',
        component: HeaderMainComponent,
        outlet: 'header',
      },
      {
        path: '',
        component: SidebarMainComponent,
        outlet: 'sidebar',
      },
    ],
  },
  {
    path: 'student-edit-form/:id',
    children: [
      {
        path: '',
        component: StudentEditFormComponent,
      },
      {
        path: '',
        component: HeaderMainComponent,
        outlet: 'header',
      },
      {
        path: '',
        component: SidebarMainComponent,
        outlet: 'sidebar',
      },
    ],
  },
  {
    path: 'achievement-list',
    children: [
      {
        path: '',
        component: AchievementListComponent,
      },
      {
        path: '',
        component: HeaderMainComponent,
        outlet: 'header',
      },
      {
        path: '',
        component: SidebarMainComponent,
        outlet: 'sidebar',
      },
    ],
  },
  {
    path: 'achievement-add-form',
    children: [
      {
        path: '',
        component: AchievementAddFormComponent,
      },
      {
        path: '',
        component: HeaderMainComponent,
        outlet: 'header',
      },
      {
        path: '',
        component: SidebarMainComponent,
        outlet: 'sidebar',
      },
    ],
  },
  {
    path: 'achievement-edit-form/:id',
    children: [
      {
        path: '',
        component: AchievementEditFormComponent,
      },
      {
        path: '',
        component: HeaderMainComponent,
        outlet: 'header',
      },
      {
        path: '',
        component: SidebarMainComponent,
        outlet: 'sidebar',
      },
    ],
  },
  {
    path: 'school-list',
    children: [
      {
        path: '',
        component: SchoolListComponent,
      },
      {
        path: '',
        component: HeaderMainComponent,
        outlet: 'header',
      },
      {
        path: '',
        component: SidebarMainComponent,
        outlet: 'sidebar',
      },
    ],
  },
  {
    path: 'school-add-form',
    children: [
      {
        path: '',
        component: SchoolAddFormComponent,
      },
      {
        path: '',
        component: HeaderMainComponent,
        outlet: 'header',
      },
      {
        path: '',
        component: SidebarMainComponent,
        outlet: 'sidebar',
      },
    ],
  },
  {
    path: 'school-edit-form/:id',
    children: [
      {
        path: '',
        component: SchoolEditFormComponent,
      },
      {
        path: '',
        component: HeaderMainComponent,
        outlet: 'header',
      },
      {
        path: '',
        component: SidebarMainComponent,
        outlet: 'sidebar',
      },
    ],
  },
  {
    path: 'sport-type-list',
    children: [
      {
        path: '',
        component: SportTypeListComponent,
      },
      {
        path: '',
        component: HeaderMainComponent,
        outlet: 'header',
      },
      {
        path: '',
        component: SidebarMainComponent,
        outlet: 'sidebar',
      },
    ],
  },
  {
    path: 'sport-type-add-form',
    children: [
      {
        path: '',
        component: SportTypeAddFormComponent,
      },
      {
        path: '',
        component: HeaderMainComponent,
        outlet: 'header',
      },
      {
        path: '',
        component: SidebarMainComponent,
        outlet: 'sidebar',
      },
    ],
  },
  {
    path: 'sport-type-edit-form/:id',
    children: [
      {
        path: '',
        component: SportTypeEditFormComponent,
      },
      {
        path: '',
        component: HeaderMainComponent,
        outlet: 'header',
      },
      {
        path: '',
        component: SidebarMainComponent,
        outlet: 'sidebar',
      },
    ],
  },
  {
    path: 'report-student',
    children: [
      {
        path: '',
        component: ReportStudentComponent,
      },
      {
        path: '',
        component: HeaderMainComponent,
        outlet: 'header',
      },
      {
        path: '',
        component: SidebarMainComponent,
        outlet: 'sidebar',
      },
    ],
  },
  {
    path: 'report-achievement',
    children: [
      {
        path: '',
        component: ReportAchievementComponent,
      },
      {
        path: '',
        component: HeaderMainComponent,
        outlet: 'header',
      },
      {
        path: '',
        component: SidebarMainComponent,
        outlet: 'sidebar',
      },
    ],
  },
];

module.exports = { routes };
