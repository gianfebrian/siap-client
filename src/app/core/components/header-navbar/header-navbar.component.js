
const { Component } = require('@angular/core');

const template = require('./header-navbar.template.html');

class HeaderNavbarComponent {}

HeaderNavbarComponent.annotations = [
  new Component({
    template,
    selector: '[header-navbar]',
  }),
];

module.exports = { HeaderNavbarComponent };
