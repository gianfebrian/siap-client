
const { Component } = require('@angular/core');

const template = require('./sidebar-user-panel.template.html');

class SidebarUserPanelComponent {}

SidebarUserPanelComponent.annotations = [
  new Component({
    template,
    selector: '[sidebar-user-panel]',
  }),
];

module.exports = { SidebarUserPanelComponent };
