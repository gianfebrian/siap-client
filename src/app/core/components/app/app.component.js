/* global $ */

const { Component } = require('@angular/core');
const { Router, RoutesRecognized } = require('@angular/router');

const template = require('./app.template.html');

class AppComponent {
  constructor(router) {
    router.events.subscribe((e) => {
      if (e instanceof RoutesRecognized) {
        if (e.url === '/login' || e.url === '/') {
          $('body').attr('class', 'hold-transition login-page');
        } else {
          $('body').attr('class', 'skin-blue fixed sidebar-mini');
        }
      }
    });
  }
}

AppComponent.annotations = [
  new Component({
    template,
    selector: 'body',
  }),
];

AppComponent.parameters = [
  [Router],
];

module.exports = { AppComponent };
