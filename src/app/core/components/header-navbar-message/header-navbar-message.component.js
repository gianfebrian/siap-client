
const { Component } = require('@angular/core');

const template = require('./header-navbar-message.template.html');

class HeaderNavbarMessageComponent {}

HeaderNavbarMessageComponent.annotations = [
  new Component({
    template,
    selector: '[header-navbar-message]',
  }),
];

module.exports = { HeaderNavbarMessageComponent };
