
const { Component } = require('@angular/core');

const template = require('./header-navbar-task.template.html');

class HeaderNavbarTaskComponent {}

HeaderNavbarTaskComponent.annotations = [
  new Component({
    template,
    selector: '[header-navbar-task]',
  }),
];

module.exports = { HeaderNavbarTaskComponent };
