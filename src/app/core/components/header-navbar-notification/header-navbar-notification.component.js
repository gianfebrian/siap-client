
const { Component } = require('@angular/core');

const template = require('./header-navbar-notification.template.html');

class HeaderNavbarNotificationComponent {}

HeaderNavbarNotificationComponent.annotations = [
  new Component({
    template,
    selector: '[header-navbar-notification]',
  }),
];

module.exports = { HeaderNavbarNotificationComponent };
