
const { Component } = require('@angular/core');
const { ActivatedRoute, Router } = require('@angular/router');

const { BehaviorSubject } = require('rxjs/BehaviorSubject');

const { SchoolService } = require('./../../services/school.service');

const template = require('./school-edit-form.template.html');

class SchoolEditFormComponent {
  constructor(schoolService, activatedRoute, router) {
    this.activatedRoute = activatedRoute;
    this.router = router;
    this.schoolService = schoolService;

    this.schoolId = null;
    this.data = new BehaviorSubject({});
  }

  ngOnInit() {
    this.activatedRoute.params
      .subscribe((params) => {
        this.schoolId = params.id;
        this.schoolService.getSchoolById(this.schoolId)
          .subscribe(result => this.data.next(result.data));
      });
  }

  onSave(data) {
    this.schoolService.updateSchool(this.schoolId, data).subscribe((result) => {
      if (result) {
        this.router.navigate(['/school-list']);
      }
    });
  }
}

SchoolEditFormComponent.annotations = [
  new Component({
    template,
    selector: 'school-edit-form',
  }),
];

SchoolEditFormComponent.parameters = [
  [SchoolService],
  [ActivatedRoute],
  [Router],
];

module.exports = { SchoolEditFormComponent };
