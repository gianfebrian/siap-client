
const { Router } = require('@angular/router');
const { Component } = require('@angular/core');

const { SchoolService } = require('./../../services/school.service');

const template = require('./school-add-form.template.html');

class SchoolAddFormComponent {
  constructor(schoolService, router) {
    this.schoolService = schoolService;

    this.router = router;
  }

  onSave(data) {
    this.schoolService.addSchool(data).subscribe((result) => {
      if (result) {
        this.router.navigate(['/school-list']);
      }
    });
  }
}

SchoolAddFormComponent.annotations = [
  new Component({
    template,
    selector: 'school-add-form',
  }),
];

SchoolAddFormComponent.parameters = [
  [SchoolService],
  [Router],
];

module.exports = { SchoolAddFormComponent };
