
const _ = require('lodash');

const { Router } = require('@angular/router');
const { Component } = require('@angular/core');
const { BehaviorSubject } = require('rxjs/BehaviorSubject');

const { SchoolService } = require('./../../services/school.service');

const template = require('./school-list.template.html');

class SchoolListComponent {
  constructor(router, schoolService) {
    this.selected = [];
    this.infoMessage = null;
    this.rows = new BehaviorSubject([]);

    this.router = router;

    this.schoolService = schoolService;
  }

  ngOnInit() {
    this.getTableData();
  }

  getTableData() {
    this.schoolService.getListOfSchool().subscribe((result) => {
      if (_.isEmpty(result.data)) {
        return this.rows;
      }

      // reset selected checkbox
      this.selected.splice(0, this.selected.length);
      return this.rows.next(result.data);
    });
  }

  onSelect({ selected } = {}) {
    this.selected.splice(0, this.selected.length);

    this.selected.push(...selected);
  }

  onEditButtonClick() {
    if (this.selected.length === 0) {
      this.infoMessage = 'Silahkan memilih data untuk melakukan perubahan';
      return;
    }

    if (this.selected.length > 1) {
      this.infoMessage = 'Anda memilih lebih dari 1 data. Jumlah pilihan yang diijinkan adalah 1 data.';
      return;
    }

    const selected = _.first(this.selected);
    this.router.navigate([`/school-edit-form/${selected.school_id}`]);
  }

  onRemoveButtonClick() {
    if (this.selected.length === 0) {
      this.infoMessage = 'Silahkan memilih data untuk melakukan penghapusan';
    }

    const ids = _.map(this.selected, 'school_id');
    this.schoolService.deleteSchool(ids)
      .subscribe(() => this.getTableData());
  }
}

SchoolListComponent.annotations = [
  new Component({
    template,
    selector: 'school-list',
  }),
];

SchoolListComponent.parameters = [
  [Router],
  [SchoolService],
];

module.exports = { SchoolListComponent };
