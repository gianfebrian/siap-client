
const { Component, EventEmitter } = require('@angular/core');
const { FormBuilder, Validators } = require('@angular/forms');

const template = require('./school-form.template.html');

class SchoolFormComponent {
  constructor(formBuilder) {
    this.data = null;
    this.onSave = new EventEmitter();

    this.schoolForm = formBuilder.group({
      name: ['', [Validators.required]],
    });
  }

  ngOnChanges(changedValue) {
    const { name } = changedValue.data.currentValue;
    this.schoolForm.controls.name.setValue(name);
  }

  onSubmit(data) {
    this.onSave.emit(data);
  }
}

SchoolFormComponent.annotations = [
  new Component({
    template,
    selector: 'school-form',
    inputs: [
      'data',
    ],
    outputs: [
      'onSave',
    ],
  }),
];

SchoolFormComponent.parameters = [
  [FormBuilder],
];

module.exports = { SchoolFormComponent };
