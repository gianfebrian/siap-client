
const _ = require('lodash');

const { Injectable } = require('@angular/core');
const { Http } = require('@angular/http');

const { RequestService } = require('./../../auth/services/request/request.service');

class SchoolService {
  constructor(http, requestService) {
    this.http = http;
    this.requestService = requestService;

    this.headers = this.requestService.getAuthHeaders();
  }

  addSchool(data) {
    const payload = JSON.stringify(data);

    return this.http
      .post('/school', payload, { headers: this.headers })
      .map(res => res.json())
      .map(res => res.success);
  }

  getListOfSchool() {
    return this.http
      .get('/school', { headers: this.headers })
      .map(res => res.json())
      .map((res) => {
        const { success, data } = res;

        return { success, data };
      });
  }

  getSchoolById(id) {
    return this.http
      .get(`/school/${id}`, { headers: this.headers })
      .map(res => res.json())
      .map((res) => {
        const { success, data } = res;

        return { success, data };
      });
  }

  updateSchool(id, data) {
    const payload = JSON.stringify(data);

    return this.http
      .put(`/school/${id}`, payload, { headers: this.headers })
      .map(res => res.json())
      .map(res => res.success);
  }

  getSelectionOptions() {
    return this.getListOfSchool().map((result) => {
      if (_.isEmpty(result.data)) {
        return [];
      }

      const options = _.map(result.data, dataObject => (
        {
          label: dataObject.name,
          value: dataObject.school_id,
        }
      ));

      return options;
    });
  }

  deleteSchool(ids = []) {
    const payload = JSON.stringify({ ids });

    return this.http
      .post('/school/delete', payload, { headers: this.headers })
      .map(res => res.json())
      .map(res => res.success);
  }
}

SchoolService.annotations = [
  new Injectable(),
];

SchoolService.parameters = [
  [Http],
  [RequestService],
];

module.exports = { SchoolService };
