
const { SchoolListComponent } = require('./components/school-list/school-list.component');
const { SchoolFormComponent } = require('./components/school-form/school-form.component');
const { SchoolAddFormComponent } = require('./components/school-add-form/school-add-form.component');
const { SchoolEditFormComponent } = require('./components/school-edit-form/school-edit-form.component');

const { SchoolService } = require('./services/school.service');

module.exports = {
  SCHOOL_DECLARATIONS: [
    SchoolListComponent,
    SchoolFormComponent,
    SchoolAddFormComponent,
    SchoolEditFormComponent,
  ],

  SCHOOL_PROVIDERS: [
    SchoolService,
  ],
};
