
const { NgModule } = require('@angular/core');
const { HttpModule } = require('@angular/http');
const { RouterModule } = require('@angular/router');
const { BrowserModule } = require('@angular/platform-browser');
const { FormsModule, ReactiveFormsModule } = require('@angular/forms');

const { NgxDatatableModule } = require('@swimlane/ngx-datatable');
const { SelectModule } = require('ng-select');
const { NKDatetimeModule } = require('ng2-datetime/ng2-datetime');
const { FileUploadModule } = require('ng2-file-upload');

const { AppComponent, CORE_DECLARATIONS, CORE_PROVIDERS } = require('./core');
const { AUTH_DECLARATIONS, AUTH_PROVIDERS } = require('./auth');
const { PAGE_DECLARATIONS } = require('./pages');

const { STUDENT_DECLARATIONS, STUDENT_PROVIDERS } = require('./student');
const { ACHIEVEMENT_DECLARATIONS, ACHIEVEMENT_PROVIDERS } = require('./achievement');
const { SCHOOL_DECLARATIONS, SCHOOL_PROVIDERS } = require('./school');
const { SPORT_TYPE_DECLARATIONS, SPORT_TYPE_PROVIDERS } = require('./sport-type');
const { REPORT_DECLARATIONS, REPORT_PROVIDERS } = require('./report');

const { routes } = require('./core/app.routes');

class AppModule {}

AppModule.annotations = [
  new NgModule({
    declarations: [
      AppComponent,
      CORE_DECLARATIONS,
      AUTH_DECLARATIONS,
      PAGE_DECLARATIONS,
      STUDENT_DECLARATIONS,
      ACHIEVEMENT_DECLARATIONS,
      SCHOOL_DECLARATIONS,
      SPORT_TYPE_DECLARATIONS,
      REPORT_DECLARATIONS,
    ],
    providers: [
      CORE_PROVIDERS,
      AUTH_PROVIDERS,
      STUDENT_PROVIDERS,
      ACHIEVEMENT_PROVIDERS,
      SCHOOL_PROVIDERS,
      SPORT_TYPE_PROVIDERS,
      REPORT_PROVIDERS,
    ],
    imports: [
      BrowserModule,
      HttpModule,
      FormsModule,
      ReactiveFormsModule,
      NgxDatatableModule,
      SelectModule,
      NKDatetimeModule,
      FileUploadModule,
      RouterModule.forRoot(routes, {
        useHash: true,
      }),
    ],
    bootstrap: [AppComponent],
  }),
];

module.exports = { AppModule };
