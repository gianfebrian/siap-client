
const _ = require('lodash');
const { Injectable } = require('@angular/core');
const { Http } = require('@angular/http');
const { BehaviorSubject } = require('rxjs/BehaviorSubject');

const { StorageService } = require('./../storage/storage.service');
const { RequestService } = require('./../request/request.service');

class UserService {
  constructor(http, storageService, requestService) {
    this.loggedIn = new BehaviorSubject(false);

    this.http = http;
    this.storageService = storageService;
    this.requestService = requestService;

    if (!_.isNull(this.storageService.getAuthToken())) {
      this.loggedIn.next(true);
    }
  }

  login(credentials) {
    const payload = JSON.stringify(credentials);
    const headers = this.requestService.getJsonHeaders();

    return this.http
      .post('/login', payload, { headers })
      .map(res => res.json())
      .map((res) => {
        if (res.success) {
          // TODO: check snake_case
          this.storageService.setAuthToken(res.authToken);
          this.loggedIn.next(true);
        }

        return res.success;
      });
  }

  logout() {
    this.storageService.removeAuthToken();
    this.loggedIn.next(false);
  }

  isLoggedIn() {
    return this.loggedIn.getValue();
  }

  getLoggedIn() {
    return this.loggedIn;
  }
}

UserService.annotations = [
  new Injectable(),
];

UserService.parameters = [
  [Http],
  [StorageService],
  [RequestService],
];

module.exports = { UserService };
