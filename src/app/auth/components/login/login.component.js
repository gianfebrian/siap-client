
const { Router } = require('@angular/router');
const { Component } = require('@angular/core');
const { FormBuilder, Validators } = require('@angular/forms');

const { UserService } = require('./../../services/user/user.service');

const template = require('./login.template.html');
const style = require('./login.style.css');

class LoginComponent {
  constructor(userService, formBuilder, router) {
    this.userService = userService;
    this.router = router;

    this.loginForm = formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  onSubmit(credentials) {
    this.userService.login(credentials).subscribe((result) => {
      if (result) {
        this.router.navigate(['/home']);
      }
    });
  }
}

LoginComponent.annotations = [
  new Component({
    template,
    selector: 'login',
    styles: [style.toString()],
  }),
];

LoginComponent.parameters = [
  [UserService],
  [FormBuilder],
  [Router],
];

module.exports = { LoginComponent };
