
const { Router } = require('@angular/router');
const { Component } = require('@angular/core');

const { StudentService } = require('./../../services/student.service');

const template = require('./student-add-form.template.html');

class StudentAddFormComponent {
  constructor(
    studentService,
    router) {
    this.studentService = studentService;

    this.router = router;
  }

  onSave(data) {
    this.studentService.addStudent(data).subscribe((result) => {
      if (result) {
        this.router.navigate(['/student-list']);
      }
    });
  }
}

StudentAddFormComponent.annotations = [
  new Component({
    template,
    selector: 'student-add-form',
  }),
];

StudentAddFormComponent.parameters = [
  [StudentService],
  [Router],
];

module.exports = { StudentAddFormComponent };
