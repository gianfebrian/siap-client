
const { Component } = require('@angular/core');
const { ActivatedRoute, Router } = require('@angular/router');

const { BehaviorSubject } = require('rxjs/BehaviorSubject');

const { StudentService } = require('./../../services/student.service');

const template = require('./student-edit-form.template.html');

class StudentEditFormComponent {
  constructor(studentService, activatedRoute, router) {
    this.activatedRoute = activatedRoute;
    this.router = router;
    this.studentService = studentService;

    this.studentId = null;
    this.data = new BehaviorSubject({});
  }

  ngOnInit() {
    this.activatedRoute.params
      .subscribe((params) => {
        this.studentId = params.id;
        this.studentService.getStudentById(this.studentId)
          .subscribe(result => this.data.next(result.data));
      });
  }

  onSave(data) {
    this.studentService.updateStudent(this.studentId, data).subscribe((result) => {
      if (result) {
        this.router.navigate(['/student-list']);
      }
    });
  }
}

StudentEditFormComponent.annotations = [
  new Component({
    template,
    selector: 'student-edit-form',
  }),
];

StudentEditFormComponent.parameters = [
  [StudentService],
  [ActivatedRoute],
  [Router],
];

module.exports = { StudentEditFormComponent };
