
const _ = require('lodash');

const { Router } = require('@angular/router');
const { Component } = require('@angular/core');
const { BehaviorSubject } = require('rxjs/BehaviorSubject');

const { StudentService } = require('./../../services/student.service');

const template = require('./student-list.template.html');

class StudentListComponent {
  constructor(router, studentService) {
    this.selected = [];
    this.infoMessage = null;
    this.rows = new BehaviorSubject([]);

    this.router = router;

    this.studentService = studentService;
  }

  ngOnInit() {
    this.getTableData();
  }

  getTableData() {
    this.studentService.getListOfStudent().subscribe((result) => {
      if (_.isEmpty(result.data)) {
        return this.rows;
      }

      // reset selected checkbox
      this.selected.splice(0, this.selected.length);
      return this.rows.next(result.data);
    });
  }

  onSelect({ selected } = {}) {
    this.selected.splice(0, this.selected.length);

    this.selected.push(...selected);
  }

  onEditButtonClick() {
    if (this.selected.length === 0) {
      this.infoMessage = 'Silahkan memilih data untuk melakukan perubahan';
    }

    if (this.selected.length > 0) {
      this.infoMessage = 'Anda memilih lebih dari 1 data. Jumlah pilihan yang diijinkan adalah 1 data.';
    }

    const selected = _.first(this.selected);
    this.router.navigate([`/student-edit-form/${selected.student_id}`]);
  }

  onRemoveButtonClick() {
    if (this.selected.length === 0) {
      this.infoMessage = 'Silahkan memilih data untuk melakukan penghapusan';
    }

    const ids = _.map(this.selected, 'student_id');
    this.studentService.deleteStudent(ids)
      .subscribe(() => this.getTableData());
  }
}

StudentListComponent.annotations = [
  new Component({
    template,
    selector: 'student-list',
  }),
];

StudentListComponent.parameters = [
  [Router],
  [StudentService],
];

module.exports = { StudentListComponent };
