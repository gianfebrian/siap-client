
const moment = require('moment');

const { Component, EventEmitter } = require('@angular/core');
const { FormBuilder, Validators } = require('@angular/forms');

const { BehaviorSubject } = require('rxjs/BehaviorSubject');

const template = require('./student-form.template.html');

const { SportTypeService } = require('./../../../sport-type/services/sport-type.service');
const { SchoolService } = require('./../../../school/services/school.service');

class StudentFormComponent {
  constructor(formBuilder, sportTypeService, schoolService) {
    this.onSave = new EventEmitter();
    this.onFormReady = new EventEmitter();

    this.sportTypeService = sportTypeService;
    this.sportTypeOptions = new BehaviorSubject([]);
    this.schoolService = schoolService;
    this.schoolOptions = new BehaviorSubject([]);

    this.datePickerOptions = {
      autoclose: true,
      format: {
        toDisplay: function toDisplay(date) {
          return moment(date).format('DD/MM/YYYY');
        },
        toValue: function toValue(date) {
          return moment(date, 'DD/MM/YYYY').toDate();
        },
      },
    };

    this.studentForm = formBuilder.group({
      fullname: ['', [Validators.required]],
      school_id: ['', [Validators.required]],
      classroom: ['', [Validators.required]],
      nisn: ['', [Validators.required]],
      birth_place: ['', [Validators.required]],
      birth_date: ['', [Validators.required]],
      address: ['', [Validators.required]],
      parent: ['', [Validators.required]],
      sport_type_id: ['', [Validators.required]],
      sport_club: ['', [Validators.required]],
    });
  }

  ngOnInit() {
    this.sportTypeService.getSelectionOptions()
      .subscribe(options => this.sportTypeOptions.next(options));

    this.schoolService.getSelectionOptions()
      .subscribe(options => this.schoolOptions.next(options));
  }

  ngOnChanges(changedValue) {
    const {
      fullname,
      school_id,
      classroom,
      nisn,
      birth_place,
      birth_date,
      address,
      parent,
      sport_type_id,
      sport_club } = changedValue.data.currentValue;

    this.studentForm.controls.fullname.setValue(fullname);
    this.studentForm.controls.classroom.setValue(classroom);
    this.studentForm.controls.nisn.setValue(nisn);
    this.studentForm.controls.birth_place.setValue(birth_place);
    this.studentForm.controls.birth_date.setValue(birth_date);
    this.studentForm.controls.address.setValue(address);
    this.studentForm.controls.parent.setValue(parent);
    this.studentForm.controls.sport_club.setValue(sport_club);
    this.studentForm.controls.school_id.setValue(String(school_id));
    this.studentForm.controls.sport_type_id.setValue(String(sport_type_id));
  }

  onSubmit(data) {
    this.onSave.emit(data);
  }
}

StudentFormComponent.annotations = [
  new Component({
    template,
    selector: 'student-form',
    inputs: [
      'data',
    ],
    outputs: [
      'onSave',
      'onFormReady',
    ],
  }),
];

StudentFormComponent.parameters = [
  [FormBuilder],
  [SportTypeService],
  [SchoolService],
];

module.exports = { StudentFormComponent };
