
const { StudentListComponent } = require('./components/student-list/student-list.component');
const { StudentFormComponent } = require('./components/student-form/student-form.component');
const { StudentAddFormComponent } = require('./components/student-add-form/student-add-form.component');
const { StudentEditFormComponent } = require('./components/student-edit-form/student-edit-form.component');

const { StudentService } = require('./services/student.service');

module.exports = {
  STUDENT_DECLARATIONS: [
    StudentListComponent,
    StudentFormComponent,
    StudentAddFormComponent,
    StudentEditFormComponent,
  ],

  STUDENT_PROVIDERS: [
    StudentService,
  ],
};
