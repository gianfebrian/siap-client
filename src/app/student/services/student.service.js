
const _ = require('lodash');

const { Injectable } = require('@angular/core');
const { Http } = require('@angular/http');

const { RequestService } = require('./../../auth/services/request/request.service');

class StudentService {
  constructor(http, requestService) {
    this.http = http;
    this.requestService = requestService;

    this.headers = this.requestService.getAuthHeaders();
  }

  addStudent(data) {
    const payload = JSON.stringify(data);

    return this.http
      .post('/student', payload, { headers: this.headers })
      .map(res => res.json())
      .map(res => res.success);
  }

  getListOfStudent() {
    return this.http
      .get('/student', { headers: this.headers })
      .map(res => res.json())
      .map((res) => {
        const { success, data } = res;

        return { success, data };
      });
  }

  getStudentById(id) {
    return this.http
      .get(`/student/${id}`, { headers: this.headers })
      .map(res => res.json())
      .map((res) => {
        const { success, data } = res;

        return { success, data };
      });
  }

  updateStudent(id, data) {
    const payload = JSON.stringify(data);

    return this.http
      .put(`/student/${id}`, payload, { headers: this.headers })
      .map(res => res.json())
      .map(res => res.success);
  }

  getSelectionOptions() {
    return this.getListOfStudent().map((result) => {
      if (_.isEmpty(result.data)) {
        return [];
      }

      const options = _.map(result.data, dataObject => (
        {
          label: dataObject.fullname,
          value: dataObject.student_id,
        }
      ));

      return options;
    });
  }

  deleteStudent(ids = []) {
    const payload = JSON.stringify({ ids });

    return this.http
      .post('/student/delete', payload, { headers: this.headers })
      .map(res => res.json())
      .map(res => res.success);
  }
}

StudentService.annotations = [
  new Injectable(),
];

StudentService.parameters = [
  [Http],
  [RequestService],
];

module.exports = { StudentService };
