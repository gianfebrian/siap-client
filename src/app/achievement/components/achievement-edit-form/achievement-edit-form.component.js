
const { Component } = require('@angular/core');
const { ActivatedRoute, Router } = require('@angular/router');

const { BehaviorSubject } = require('rxjs/BehaviorSubject');

const { AchievementService } = require('./../../services/achievement.service');

const template = require('./achievement-edit-form.template.html');

class AchievementEditFormComponent {
  constructor(achievementService, activatedRoute, router) {
    this.activatedRoute = activatedRoute;
    this.router = router;
    this.achievementService = achievementService;

    this.achievementId = null;
    this.data = new BehaviorSubject({});
  }

  ngOnInit() {
    this.activatedRoute.params
      .subscribe((params) => {
        this.achievementId = params.id;
        this.achievementService.getAchievementById(this.achievementId)
          .subscribe(result => this.data.next(result.data));
      });
  }

  onSave(data) {
    this.achievementService.updateAchievement(this.achievementId, data).subscribe((result) => {
      if (result) {
        this.router.navigate(['/achievement-list']);
      }
    });
  }
}

AchievementEditFormComponent.annotations = [
  new Component({
    template,
    selector: 'achievement-edit-form',
  }),
];

AchievementEditFormComponent.parameters = [
  [AchievementService],
  [ActivatedRoute],
  [Router],
];

module.exports = { AchievementEditFormComponent };
