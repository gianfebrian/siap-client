
const { Router } = require('@angular/router');
const { Component } = require('@angular/core');

const { AchievementService } = require('./../../services/achievement.service');

const template = require('./achievement-add-form.template.html');

class AchievementAddFormComponent {
  constructor(router, achievementService) {
    this.achievementService = achievementService;

    this.router = router;
  }

  onSave(data) {
    this.achievementService.addAchievement(data)
      .subscribe((result) => {
        if (result) {
          this.router.navigate(['/achievement-list']);
        }
      });
  }
}

AchievementAddFormComponent.annotations = [
  new Component({
    template,
    selector: 'achievement-add-form',
  }),
];

AchievementAddFormComponent.parameters = [
  [Router],
  [AchievementService],
];

module.exports = { AchievementAddFormComponent };
