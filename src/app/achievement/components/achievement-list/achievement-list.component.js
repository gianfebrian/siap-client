
const _ = require('lodash');

const { Router } = require('@angular/router');
const { Component } = require('@angular/core');
const { BehaviorSubject } = require('rxjs/BehaviorSubject');

const { AchievementService } = require('./../../services/achievement.service');

const template = require('./achievement-list.template.html');

class AchievementListComponent {
  constructor(router, achievementService) {
    this.selected = [];
    this.infoMessage = null;
    this.rows = new BehaviorSubject([]);

    this.router = router;

    this.achievementService = achievementService;
  }

  ngOnInit() {
    this.getTableData();
  }

  getTableData() {
    this.achievementService.getListOfAchievement().subscribe((result) => {
      if (_.isEmpty(result.data)) {
        return this.rows;
      }

      // reset selected checkbox
      this.selected.splice(0, this.selected.length);
      return this.rows.next(result.data);
    });
  }

  onSelect({ selected } = {}) {
    this.selected.splice(0, this.selected.length);

    this.selected.push(...selected);
  }

  onEditButtonClick() {
    if (this.selected.length === 0) {
      this.infoMessage = 'Silahkan memilih data untuk melakukan perubahan';
    }

    if (this.selected.length > 0) {
      this.infoMessage = 'Anda memilih lebih dari 1 data. Jumlah pilihan yang diijinkan adalah 1 data.';
    }

    const selected = _.first(this.selected);
    this.router.navigate([`/achievement-edit-form/${selected.achievement_id}`]);
  }

  onRemoveButtonClick() {
    if (this.selected.length === 0) {
      this.infoMessage = 'Silahkan memilih data untuk melakukan penghapusan';
    }

    const ids = _.map(this.selected, 'achievement_id');
    this.achievementService.deleteAchievement(ids)
      .subscribe(() => this.getTableData());
  }
}

AchievementListComponent.annotations = [
  new Component({
    template,
    selector: 'achievement-list',
  }),
];

AchievementListComponent.parameters = [
  [Router],
  [AchievementService],
];

module.exports = { AchievementListComponent };
