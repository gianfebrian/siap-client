
const _ = require('lodash');

const { Component, EventEmitter } = require('@angular/core');
const { FormBuilder, Validators } = require('@angular/forms');

const { BehaviorSubject } = require('rxjs/BehaviorSubject');

const { FileUploader } = require('ng2-file-upload');

const template = require('./achievement-form.template.html');
const style = require('./achievement-form.style.css');

const { SportTypeService } = require('./../../../sport-type/services/sport-type.service');
const { StudentService } = require('./../../../student/services/student.service');

class AchievementFormComponent {
  constructor(formBuilder, sportTypeService, studentService) {
    this.onSave = new EventEmitter();
    this.onFormReady = new EventEmitter();

    this.sportTypeService = sportTypeService;
    this.sportTypeOptions = new BehaviorSubject([]);
    this.studentService = studentService;
    this.studentOptions = new BehaviorSubject([]);

    this.hasBaseDropZoneOver = false;
    this.uploader = new FileUploader({ url: '/upload' });

    this.achievementForm = formBuilder.group({
      student_id: ['', [Validators.required]],
      sport_type_id: ['', [Validators.required]],
      sport_sub_type: ['', [Validators.required]],
      competition: ['', [Validators.required]],
      competition_level: ['', [Validators.required]],
      achievement: ['', [Validators.required]],
    });
  }

  ngOnInit() {
    this.sportTypeService.getSelectionOptions()
      .subscribe(options => this.sportTypeOptions.next(options));

    this.studentService.getSelectionOptions()
      .subscribe(options => this.studentOptions.next(options));
  }

  ngOnChanges(changedValue) {
    const {
      student_id,
      sport_type_id,
      sport_sub_type,
      competition,
      competition_level,
      achievement } = changedValue.data.currentValue;

    this.achievementForm.controls.student_id.setValue(String(student_id));
    this.achievementForm.controls.sport_type_id.setValue(String(sport_type_id));
    this.achievementForm.controls.sport_sub_type.setValue(sport_sub_type);
    this.achievementForm.controls.competition.setValue(competition);
    this.achievementForm.controls.competition_level.setValue(competition_level);
    this.achievementForm.controls.achievement.setValue(achievement);
  }


  onFileOver(e) {
    this.hasBaseDropZoneOver = e;
  }

  onFileDrop() {
    // as of now, we only support a single attachment.
    if (this.uploader.queue.length > 1) {
      this.uploader.queue.shift();
    }
  }

  removeUploadFile() {
    // needs to rework this if in the future we support multiple attachments.
    this.uploader.queue.shift();
  }

  onSubmit(data) {
    const uploadItem = _.first(this.uploader.queue);
    const dataWithAttachmentMedia = data;

    if (uploadItem) {
      this.uploader.uploadItem(_.first(this.uploader.queue));
      this.uploader.onCompleteItem = (item, response) => {
        const res = JSON.parse(response);

        if (res.success === true) {
          dataWithAttachmentMedia.media_id = res.data.media_id;
          this.onSave.emit(dataWithAttachmentMedia);
        }
      };

      return;
    }

    dataWithAttachmentMedia.media_id = null;

    this.onSave.emit(data);
  }
}

AchievementFormComponent.annotations = [
  new Component({
    template,
    styles: [style.toString()],
    selector: 'achievement-form',
    inputs: [
      'data',
    ],
    outputs: [
      'onSave',
      'onFormReady',
    ],
  }),
];

AchievementFormComponent.parameters = [
  [FormBuilder],
  [SportTypeService],
  [StudentService],
];

module.exports = { AchievementFormComponent };
