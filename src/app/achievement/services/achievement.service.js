
const { Injectable } = require('@angular/core');
const { Http } = require('@angular/http');

const { RequestService } = require('./../../auth/services/request/request.service');

class AchievementService {
  constructor(http, requestService) {
    this.http = http;
    this.requestService = requestService;
    this.headers = this.requestService.getAuthHeaders();
  }

  addAchievement(data) {
    const payload = JSON.stringify(data);

    return this.http
      .post('/achievement', payload, { headers: this.headers })
      .map(res => res.json())
      .map(res => res.success);
  }

  getListOfAchievement() {
    return this.http
      .get('/achievement', { headers: this.headers })
      .map(res => res.json())
      .map((res) => {
        const { success, data } = res;

        return { success, data };
      });
  }

  getAchievementById(id) {
    return this.http
      .get(`/achievement/${id}`, { headers: this.headers })
      .map(res => res.json())
      .map((res) => {
        const { success, data } = res;

        return { success, data };
      });
  }

  updateAchievement(id, data) {
    const payload = JSON.stringify(data);

    return this.http
      .put(`/achievement/${id}`, payload, { headers: this.headers })
      .map(res => res.json())
      .map(res => res.success);
  }

  deleteAchievement(ids = []) {
    const payload = JSON.stringify({ ids });

    return this.http
      .post('/achievement/delete', payload, { headers: this.headers })
      .map(res => res.json())
      .map(res => res.success);
  }
}

AchievementService.annotations = [
  new Injectable(),
];

AchievementService.parameters = [
  [Http],
  [RequestService],
];

module.exports = { AchievementService };
