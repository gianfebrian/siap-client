
const { AchievementListComponent } = require('./components/achievement-list/achievement-list.component');
const { AchievementFormComponent } = require('./components/achievement-form/achievement-form.component');
const { AchievementAddFormComponent } = require('./components/achievement-add-form/achievement-add-form.component');
const { AchievementEditFormComponent } = require('./components/achievement-edit-form/achievement-edit-form.component');

const { AchievementService } = require('./services/achievement.service');

module.exports = {
  ACHIEVEMENT_DECLARATIONS: [
    AchievementListComponent,
    AchievementFormComponent,
    AchievementAddFormComponent,
    AchievementEditFormComponent,
  ],

  ACHIEVEMENT_PROVIDERS: [
    AchievementService,
  ],
};
