require('./shim');
require('rxjs/add/operator/map');

require('@angular/core');
require('@angular/common');
require('@angular/platform-browser');
require('@angular/platform-browser-dynamic');
require('@angular/router');
require('@angular/http');
require('@angular/forms');

// CSS
require('bootstrap/dist/css/bootstrap.css');
require('bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css');
require('bootstrap-timepicker/css/bootstrap-timepicker.min.css');
require('font-awesome/css/font-awesome.css');
require('admin-lte/dist/css/alt/AdminLTE-without-plugins.css');
require('admin-lte/dist/css/skins/skin-blue.css');

require('@swimlane/ngx-datatable/release/index.css');
require('@swimlane/ngx-datatable/release/themes/material.css');
require('@swimlane/ngx-datatable/release/assets/icons.css');

require('./overrided.css');

// JS
const jquery = require('jquery');

$ = jquery; // eslint-disable-line
jQuery = jquery; // eslint-disable-line

require('bootstrap');
require('bootstrap-datepicker');
require('bootstrap-timepicker');
require('jquery-slimscroll');
require('admin-lte');
