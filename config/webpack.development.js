/* eslint-disable import/no-extraneous-dependencies */

const path = require('path');

const webpack = require('webpack');
const webpackHotMiddleware = require('webpack-hot-middleware');

const configurator = (env) => {
  const config = {
    entry: {
      vendor: path.join(env.rootDir, 'src', 'vendor.js'),
      boot: path.join(env.rootDir, 'src', 'boot.js'),
    },
    output: {
      path: path.join(env.rootDir, 'public'),
      filename: '[name].js',
    },

    module: {
      rules: [
        {
          test: /\.css$/,
          use: [
            {
              loader: 'style-loader',
            },
            {
              loader: 'css-loader',
              options: {
                alias: {
                  '../img': 'admin-lte/dist/img/',
                },
              },
            },
          ],
        },
        {
          test: /\.less$/,
          use: ['style-loader', 'css-loader', 'less-loader'],
        },
        {
          test: /\.json$/,
          use: ['json-loader'],
        },
        {
          test: /\.html$/,
          exclude: /(node_modules)/,
          use: ['raw-loader'],
        },
        {
          test: /\.(ttf|eot|svg|otf)$/,
          use: ['file-loader'],
        },
        {
          test: /\.woff(2)?$/,
          use: ['url-loader?limit=8192&minetype=application/font-woff'],
        },
        {
          test: /\.(png|jpg)$/,
          use: ['url-loader?limit=8192'],
        },
      ],

      noParse: [
        /.+zone\.js\/dist\/.+/, /.+angular2\/bundles\/.+/,
      ],
    },

    plugins: [
      new webpack.DefinePlugin({
        ENVIRONMENT: JSON.stringify('development'),
      }),
      new webpack.HotModuleReplacementPlugin(),
      new webpack.optimize.CommonsChunkPlugin({
        name: 'vendor',
        filename: 'vendor.js',
      }),
    ],

    devServer: {
      contentBase: path.join(env.rootDir, 'src'),
      compress: true,
      port: '8080',
      noInfo: false,
      historyApiFallback: false,
      watchOptions: {
        aggregateTimeout: 300,
        poll: 1000,
      },
      setup: function setup(app) {
        const compiler = webpack(config);
        app.use(webpackHotMiddleware(compiler));
      },
    },

    devtool: 'eval-source-map',
  };

  return config;
};

module.exports = configurator;
